import java.util.InputMismatchException;
import java.util.Scanner;
public class Roulette{
	
	public static void main(String[]args){
		
		//variables
		int playerNum = 0;
		Scanner scan = new Scanner(System.in);
		boolean restart = false; //for another round
		
		//player setup part 1 player count
		System.out.println("How many players? Max 5");
		boolean Valid = false;
		//validator
		while(!Valid){
			try{
				playerNum = scan.nextInt();
				if(playerNum>=1 && playerNum<=5){
					Valid = true;
				}else{
					System.out.println("That number is not between 1 and 5 buddy");
				}
			}
			catch(InputMismatchException e){
				System.out.println("That is not a number buddy");
				scan.next();
			}
		}
		
		//player setup part 2 starting money
		int[] players = new int[playerNum];
		for(int i=0; i<playerNum; i++){
			players[i] = 1000;
		}
		
		//rounds restart here
		while(!restart){
			//THE FUN PART BETTING
			int[] bets = new int[playerNum];
			for(int i=0; i<playerNum; i++){
				boolean betValid = false;
				String answer = "";
				System.out.println("Player "+(i+1)+": "+players[i]+"$");
				//validating
				boolean yn = false;
				while(!yn){
					System.out.println("Would you like to bet? (yes/no)");
					answer = scan.next();
					if(answer.equals("no") || answer.equals("yes")){
						yn = true;
					}
				}
				if(answer.equals("no") || players[i] == 0){
					bets[i] = 0;
					betValid = true;
				}
				while(!betValid){
					try{
						System.out.println("How much would you like to bet?");
						int max = players[i];
						bets[i] = scan.nextInt();
						if(bets[i]>=1 && bets[i]<=max){
							betValid = true;
						}else{
							System.out.println("Amount not valid");
						}
					}
					catch(InputMismatchException e){
					System.out.println("That is not a number buddy");
					scan.next();
					}
				}
			}
			
			//Which number
			int[] betChoice = new int[playerNum];
			for(int i=0; i<playerNum; i++){
				boolean betChoiceValid = false;
				if(bets[i] == 0){
					betChoice[i] = -1;
				}else{
					System.out.println("Player "+(i+1)+" on which number would you like to bet?");
					//validating
					while(!betChoiceValid){
						try{
							betChoice[i] = scan.nextInt();
							if(betChoice[i]>=0 && betChoice[i]<=36){
								betChoiceValid = true;
							}else{
								System.out.println("Bet not valid");
							}
						}
						catch(InputMismatchException e){
						System.out.println("That is not a number buddy");
						scan.next();
						}
					}
				}	
			}
			
			//bet multiplicator
			int multiplicator = 35;
			
			//Spining the wheel
			RouletteWheel wheel = new RouletteWheel();
			wheel.spin();
			int value = wheel.getValue();
			System.out.println("And the wheel lands on..... "+value+"!!!");
			
			//Rewarding
			for(int i=0; i<playerNum; i++){
				if(value == betChoice[i]){
					int winnings = bets[i]*multiplicator;
					players[i] = players[i]+winnings;
					System.out.println("Player "+(i+1)+" you won "+winnings+"$");
				}else{
					players[i] = players[i]-bets[i];
					System.out.println("Player "+(i+1)+" you lost "+bets[i]+"$");
				}
			}
			
			//another round??
			String again = "";
			boolean round = false;
			while(!round){
				System.out.println("would everyone like to play another round? (yes/no)");
				again = scan.next();
				if(again.equals("no") || again.equals("yes")){
					round = true;
				}
			}
			if(again.equals("no") || (players[0] == 0 && players[1] == 0 && players[2] == 0 && players[3] == 0 && players[4] == 0)){
				for(int i=0; i<playerNum; i++){
					int totalCalculator = players[i]-1000;
					System.out.println("Player "+(i+1)+" you won a total of "+totalCalculator+"$");
				}
				restart = true;
			}
		}
	}
}